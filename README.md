# Steampunk

#1.Overview

This project exist to entertain and educate developers from Tryzens, so no stress.

#2.Technologies

Many things are yet to be discussed and approved with the participants, like the technologies used, depth of gameplay, interface, graphics etc.

The first proposition is to use Node.js with whatever libraries we find fit as it is what we are all most familiar with and is going to reduce the learning curve on the project substantionaly.

A comunication channel should be created for the participants as well as a story tracking environment. Both of these should be discussed as soon as possible, so work on the project can start can start

(setup guide for the project will be inserted here at some point)

#3.Gameplay (concepts)

The basic idea of the game is to create a vehicles and with it, fight the vehicles of other players.

The development can be separated into three distinct phases, but should not be limited to them as more can arise as the concept of the game matures.

  1.Vehicle creator interface

  2.Combat mechanics

  3.Multiplayer

#4.Moving Forward (TODO)

A simple editor (html, css, & js will suffice) for the JSON (presumably ???) file holding the structure of the vehicle.
This will later evolve in the vehicle creator interface.
